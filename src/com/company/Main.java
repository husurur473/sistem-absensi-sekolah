package com.company;
import java.io.*;
import java.time.LocalDate;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;


public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner input = new Scanner(System.in);
        Siswa[] siswa = new Siswa[10];
        siswa[0] = new Siswa( "Agung Husurur ", "G", 30001, "Laki-Laki");
        siswa[1] = new Siswa( "Anselma Putri", "G", 30002, "Perempuan");
    int pil;
        System.out.println("-====== Sistem Absensi Siswa ======-");
        System.out.println("Masuk Sebagai : ");
        System.out.println("\t1. Siswa");
        System.out.println("\t2. Admin");
        System.out.print("Masukkan Pilihan : ");
        pil = input.nextInt();

    if (pil == 1){
        try {
            int NISN2;
            int i;
            System.out.print("Masukkan NISN Anda : ");
            NISN2 = input.nextInt();
                    //Menulis Ke File txt.
                    try (FileWriter f = new FileWriter("filename.txt", true);
                         BufferedWriter b = new BufferedWriter(f);
                         PrintWriter p = new PrintWriter(b)){
                        try {
                            for (i = 0; i < 10 ; i++) {
                                if (siswa[i].getNISN() == NISN2) {
                                    System.out.println("Selamat Datang " + siswa[i].getNama());
                                    System.out.println("Semangat Sekolah nya yaa :).....");
                                    LocalDate obj = LocalDate.now();
                                    LocalDateTime myDateObj = LocalDateTime.now();
                                    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss");
                                    String formattedDate = myDateObj.format(myFormatObj);
                                    String Kehadiran = "Hadir";
                                    p.println("Nama : " + siswa[i].getNama());
                                    p.println("Kelas : " + siswa[i].getKelas());
                                    p.println("Tanggal : " + obj);
                                    p.println("Waktu : " + formattedDate);
                                    p.println("Keterangan : " + Kehadiran);
                                    p.println("===================================");
                                    break;

                                }

                            }

                        }catch (NullPointerException r){
                            System.out.println("NISN Tidak Ditemukan.");
                        }
                }
        }catch(Exception z){
            z.printStackTrace();
        }

    }if (pil == 2){
            int admin = 12345; // ID Admin
            int idAdmin;
            System.out.print("Masukkan ID Admin : " );
            idAdmin = input.nextInt();
            if (idAdmin == admin){
                int pil3;
                System.out.println("1. Melihat Data Absensi Hari Ini ");
                System.out.println("2. Melihat Data Siswa");
                System.out.print("Masukkan Pilihan : ");
                pil3 = input.nextInt();
                if (pil3 == 1){
                    System.out.println("-====== Data Absensi Siswa ======-");
                    String fileName = "filename.txt" ;

                    try {
                        // membaca file
                        File myFile = new File(fileName);
                        Scanner fileReader = new Scanner(myFile);

                        // cetak isi file
                        while(fileReader.hasNextLine()){
                            String data = fileReader.nextLine();
                            System.out.println(data);
                        }

                    } catch (FileNotFoundException e) {
                        System.out.println("Terjadi Kesalahan: " + e.getMessage());
                        e.printStackTrace();
                    }
                }else if (pil3 == 2){
                    int NISN2;
                    System.out.print("Masukkan NISN Siswa : ");
                    NISN2 = input.nextInt();
                    try {
                        for (int i = 0; i < 10 ; i++) {
                            if (siswa[i].getNISN() == NISN2) {
                                System.out.println("Nama : " + siswa[i].getNama());
                                System.out.println("Jenis Kelamin : " + siswa[i].getJenisKelamin());
                                System.out.println("Kelas : " + siswa[i].getKelas());
                                System.out.println("NISN : " + siswa[i].getNISN());
                                break;

                            }

                        }

                    }catch (NullPointerException r){
                        System.out.println("NISN Tidak Ditemukan.");
                    }

                }
            }



        }
    }
}
