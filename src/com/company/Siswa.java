package com.company;


public class Siswa {
    String nama;
    String kelas;
    int NISN;
    String jenisKelamin;

    public Siswa( String nama, String kelas, int NISN, String jenisKelamin) {

        this.nama = nama;
        this.kelas = kelas;
        this.NISN = NISN;
        this.jenisKelamin = jenisKelamin;
    }

    public String getNama() {
        return nama;
    }

    public String getKelas() {
        return kelas;
    }

    public int getNISN() {
        return NISN;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }
}

